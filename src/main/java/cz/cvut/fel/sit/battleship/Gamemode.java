package cz.cvut.fel.sit.battleship;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Properties;

import cz.cvut.fel.sit.battleship.ShipTypes.AircraftCarrier;
import cz.cvut.fel.sit.battleship.ShipTypes.Battleship;
import cz.cvut.fel.sit.battleship.ShipTypes.Convoy;
import cz.cvut.fel.sit.battleship.ShipTypes.Cruiser;
import cz.cvut.fel.sit.battleship.ShipTypes.Destroyer;
import cz.cvut.fel.sit.battleship.ShipTypes.Ship;

public abstract class Gamemode {

    public static int size;
    public static Ship[] ships;

// SETTERS and GETTERS
    public static void setSize(int size) {
        Gamemode.size = size;
    }

    public static int getSize() {
        return size;
    }

    public static Ship[] getShips() {
        return ships;
    }

    public static void setShips(Ship[] ships) {
        Gamemode.ships = ships;
    }

    public class getPropertyValues {
        String result = "";
        InputStream inputStream;
     
        public boolean getPropValues() throws IOException 
        {
            try {
                Properties prop = new Properties();
                String propFileName = "config.properties";
     
                inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
     
                if (inputStream != null) {
                    prop.load(inputStream);
                } else {
                    throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
                }
     
                // get the size and ships from prop file
                int propSize = Integer.valueOf(prop.getProperty("size"));
                setSize(propSize);

                String propShipsString = prop.getProperty("ships");
                String[] propShipArray = propShipsString.split(",");
                ArrayList<Ship> shiplist = new ArrayList<>();

                for (int i = 0; i < propShipArray.length; i++){
                    switch (propShipArray[i]){

                        case "Convoy":
                            shiplist.add(new Convoy());
                            break;

                        case "Destroyer":
                            shiplist.add(new Destroyer());
                            break;

                        case "Cruiser":
                            shiplist.add(new Cruiser());
                            break;

                        case "Battleship":
                            shiplist.add(new Battleship());
                            break;

                        case "AircraftCarrier":
                            shiplist.add(new AircraftCarrier());
                            break;

                    }
                }

                Ship[] propShips = shiplist.toArray(new Ship[0]);
                setShips(propShips);

            } catch (Exception e) {

                System.out.println("Exception: " + e);
                return false;

            } finally {

                inputStream.close();

            }

            return true;
        }
    }
}
