package cz.cvut.fel.sit.battleship.GameUsers;

import java.util.Random;

import cz.cvut.fel.sit.battleship.GameField.*;
import cz.cvut.fel.sit.battleship.ShipTypes.*;

public class AI {

    private Player local;
    private Board enemyBoard;

    

    public AI(Player local, Board enemyBoard) {
        this.local = local;
        this.enemyBoard = enemyBoard;
    }

    public boolean isValid(Square choosenSquare)
    {        
        if((choosenSquare.getStatus() == SquareStatus.Water || choosenSquare.getStatus() == SquareStatus.Ship || choosenSquare.getStatus() == SquareStatus.TooClose))
            return true;

        return false;
    }

    public void placeAllShips(Ship[] gamemodeShips)
    {
        int[] target = new int[2];
        int orient;
        Orientation orientation;

        for(int i = 0; i < gamemodeShips.length; i++)
            do{
                //set Board
                gamemodeShips[i].board = local.field;

                Random r = new Random();
                target[0] = r.nextInt(local.field.sideSize);
                target[1] = r.nextInt(local.field.sideSize);
                //set Random Position
                gamemodeShips[i].setFstSquare(local.field.getPosition(target[0],target[1]));

                orient = r.nextInt(2);
                orientation = (orient==0) ? Orientation.horizontal : Orientation.vertical;
                //set Random Orientation
                gamemodeShips[i].setOrientation(orientation);

            }while(!gamemodeShips[i].placeShip());
    }

    public boolean fireAI(){
       while (true)
       {
           int[] target = new int[2];
           Random r = new Random();
           target[0] = r.nextInt(local.field.sideSize);
           target[1] = r.nextInt(local.field.sideSize);
           Square strikeField = local.field.getPosition(target[0],target[1]);
           //Infinity loop for random firestrikes
           if (isValid(strikeField)){
               return local.fire(enemyBoard, strikeField);
           }
       }
    }
}