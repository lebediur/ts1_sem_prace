package cz.cvut.fel.sit.battleship.GameField;

import cz.cvut.fel.sit.battleship.Gamemode;

public class Board {
    public int sideSize = Gamemode.size;
    private Square[][] board = new Square[sideSize][sideSize];
    private boolean visible;

    public void setSideSize(int sideSize) {
        this.sideSize = sideSize;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public int getSideSize() {
        return sideSize;
    }

    public Square[][] getBoard() {
        return board;
    }

    public void setBoard(Square[][] board) {
        this.board = board;
    }

    public boolean isVisible() {
        return visible;
    }

    //Constructor
    public Board boardConstuction(boolean visibile) {
        this.visible = visibile;
        for (int i = 0; i < sideSize; i++) {
            for (int j = 0; j < sideSize; j++) {
                board[i][j] = new Square(i, j, SquareStatus.Water);
            }
        }
        return this;
    }

    //Getter position
    public Square getPosition(int x, int y) {
        return board[x][y];
    }

    //Check for any alive ships
    public boolean allShipsSunk() {
        for (int i = 0; i < sideSize; i++)
            for (int j = 0; j < sideSize; j++)
                if (board[i][j].getStatus() == SquareStatus.Ship)
                    return false;
        return true;
    }
}

