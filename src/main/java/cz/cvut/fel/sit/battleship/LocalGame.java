package cz.cvut.fel.sit.battleship;

import cz.cvut.fel.sit.battleship.GameUsers.*;
import cz.cvut.fel.sit.battleship.ShipTypes.*;

import java.util.*;

public class LocalGame {
    private final static int size = Gamemode.size;
    private static Scanner in;
    private static Player player, localEnemy;
    private static AI ai;
    public static Ship[] playersShips;
    public static Ship[] aiShips;
    public static void main(String[] args)
    {
        
        System.arraycopy(Gamemode.ships, 0, playersShips, 0, Gamemode.ships.length);
        System.arraycopy(Gamemode.ships, 0, aiShips, 0, Gamemode.ships.length);
        player = new Player("Player");
        player.setVisible(true);
        localEnemy = new Player("AI");
        localEnemy.setVisible(false);

        ai = new AI(localEnemy, player.field);
        ai.placeAllShips(aiShips);


        try{
            in = new Scanner(System.in);
            placePlayerShips();
            gameLoop();
            printResults();
            in.close();
        }
        catch(Exception e)
        { 
            System.err.println(e); 
        }
    }

    public static int[] getInput()
    {
        int[] coordinates = new int[2];
        coordinates[0] = in.nextInt();
        coordinates[1] = in.nextInt();
        while(coordinates[0] < 0 || coordinates[0] > size - 1 || coordinates[1] < 0 || coordinates[1] > size - 1 )
        {
            coordinates[0] = in.nextInt();
            coordinates[1] = in.nextInt();
        }

        return coordinates;
    }

    public static Orientation getOrientation()
    {
        System.out.println("To change orientation press \"R\"");
        char ans = in.next().charAt(0);

        if(ans != 'r' || ans != 'R')
            return Orientation.vertical;
        else
            return Orientation.horizontal;
    }

    public static void placePlayerShips(){
        int[] coordinates;
        Orientation orientation;
        for(int i = 0; i<5; i++)
        {
            do{
                System.out.println("For the ship " + (i+1) + " with size " + playersShips[i].getSize());
                coordinates = getInput();
                
                playersShips[i].setFstSquare(player.field.getPosition(coordinates[0], coordinates[1]));
                
                orientation = getOrientation();
                
                playersShips[i].setOrientation(orientation);

            }while(!playersShips[i].placeShip());
            
        }
    }


    public static void gameLoop(){
        while( !player.field.allShipsSunk() && !localEnemy.field.allShipsSunk())
        {
            int[] target = new int[2];  
            do{    
                do{
                    System.out.println("Give x and y to hit");
                    target[0] = in.nextInt();
                    target[1] = in.nextInt();
                }while(target[0] < 0 || target[0]> size - 1 || target[1] < 0 || target[1] > size - 1);
                
            }while(player.fire(localEnemy.field, localEnemy.field.getPosition(target[0], target[1])));

            if(localEnemy.field.allShipsSunk()){
                break;
            }
            while(ai.fireAI());
        }
    }

    public static void printResults()
    {
        if(player.getField().allShipsSunk())
            System.out.println("\n-- THE WINNER IS PC! --");
        else if(localEnemy.getField().allShipsSunk())
            System.out.println("\n-- THE WINNER IS PLAYER! --");
    }
    
}
