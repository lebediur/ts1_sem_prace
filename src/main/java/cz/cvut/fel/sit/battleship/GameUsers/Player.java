package cz.cvut.fel.sit.battleship.GameUsers;

import cz.cvut.fel.sit.battleship.GameField.Board;
import cz.cvut.fel.sit.battleship.GameField.Square;
import cz.cvut.fel.sit.battleship.GameField.SquareStatus;



public class Player {
    public String name;
    private int turns = 0;
    public int strikes = 0;
    public int misses = 0;
    private boolean visible;
    public Board field = new Board();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getStrikes() {return strikes;}

    public int getMisses() {return misses;}

    public Board getField() {
        return field;
    }

    public void setField(Board field) {
        this.field = field;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public Player(String name) {
        this.name = name;
        this.field.boardConstuction(this.visible);
    }


    public void isFiredSquare(Board board, int x, int y) throws RepeatedStrikeException {
        if((board.getPosition(x,y).getStatus() == SquareStatus.Miss)||(board.getPosition(x,y).getStatus() == SquareStatus.BombedShip)) {
            throw new RepeatedStrikeException();
        }
    }

    public boolean fire(Board board, Square square){
        int  x = square.getX();
        int y = square.getY();
        try {
            isFiredSquare(board, x, y);

            if (square.getStatus() == SquareStatus.Ship){
                square.setStatus(SquareStatus.BombedShip);
                strikes++;
                return true;
            } else if ((square.getStatus() == SquareStatus.Water)||(square.getStatus() == SquareStatus.TooClose)){
                square.setStatus(SquareStatus.Miss);
                misses++;
                return false;
            }
            return true;
        }
        catch (Exception e){

            System.err.println(e);
            return false;
        }
    }

    //Statistic for player's motivation :D
    public String getStatsString() {
        return("Player:" + getName() + "\nstrikes: " + getStrikes() + "\nmisses: " + getMisses());
    }

}
