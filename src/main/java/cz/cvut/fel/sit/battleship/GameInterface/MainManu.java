package cz.cvut.fel.sit.battleship.GameInterface;

import cz.cvut.fel.sit.battleship.GameInterface.UIFactory.UIButtonFactory;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.stage.StageStyle;


public class MainManu extends Application {
    public static final int width = 900;
    public static final int height = 600;

    @Override
    public void start(Stage stage) {
        stage.setWidth(width);
        stage.setHeight(height);

        //Stack pane for stage
        StackPane stackPane = new StackPane();
        stackPane.setPrefSize(height, width);

        //Box and label for title
        VBox titleBox = new VBox();
        Label title = new Label();
        title.setText("DREADNOUGHT");
        title.paddingProperty().setValue(new Insets(20));
        title.setFont(Font.font("Comic Sans MS", 40));
        title.setBackground(UIConstants.DARK_BUTTON_BACKGROUND);
        title.setTextFill(Color.WHITE);
        titleBox.setPadding(new Insets(30));
        titleBox.getChildren().add(title);
        titleBox.setAlignment(Pos.TOP_CENTER);

        //Box and buttons
        VBox buttonBox = new VBox();
        buttonBox.setAlignment(Pos.CENTER);
        buttonBox.setSpacing(10);

        Button newGameManMan = UIButtonFactory.createButton("New game: man vs. man", Color.WHITE, UIConstants.LIGHT_BUTTON_BACKGROUND);
        newGameManMan.setOnMouseClicked(mouseEvent -> {

        });

        Button newGameManPC = UIButtonFactory.createButton("New game: man vs PC", Color.WHITE, UIConstants.LIGHT_BUTTON_BACKGROUND);
        newGameManPC.setOnMouseClicked(mouseEvent -> {

        });

        Button newOnlineGame = UIButtonFactory.createButton("New online game", Color.WHITE, UIConstants.LIGHT_BUTTON_BACKGROUND);
        newOnlineGame.setOnMouseClicked(mouseEvent -> {

        });

        Button signUpLoginIn = UIButtonFactory.createButton("Sign up / log in", Color.WHITE, UIConstants.LIGHT_BUTTON_BACKGROUND);
        signUpLoginIn.setOnMouseClicked(mouseEvent -> {

        });

        buttonBox.getChildren().add(newGameManMan);
        buttonBox.getChildren().add(newGameManPC);
        buttonBox.getChildren().add(newOnlineGame);
        buttonBox.getChildren().add(signUpLoginIn);

        VBox quitButtonBox = new VBox();
        Button quitButton = UIButtonFactory.createButton("Quit game", Color.WHITE, UIConstants.DARK_BUTTON_BACKGROUND);
        quitButton.setOnMouseClicked(mouseEvent -> stage.close());
        quitButtonBox.getChildren().add(quitButton);
        quitButtonBox.setPadding(new Insets(20));
        quitButtonBox.setAlignment(Pos.BOTTOM_CENTER);

        stackPane.getChildren().add(titleBox);
        stackPane.getChildren().add(buttonBox);
        stackPane.getChildren().add(quitButtonBox);
        stackPane.setAlignment(Pos.TOP_CENTER);

        Scene scene = new Scene(stackPane, width, height);
        stackPane.setBackground(UIConstants.BASE_BACKGROUND);
        stage.initStyle(StageStyle.DECORATED);
        stage.setScene(scene);
        stage.show();
    }
}
