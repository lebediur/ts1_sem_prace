package cz.cvut.fel.sit.battleship.GameConnection;
//TODO Adaptate 4 our project.
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author ladislav.seredi@fel.cvut.cz
 */
public class Connection implements Runnable {

    private final String host;
    private final int port;
    private final Role role;
    private ServerSocket ss;
    private Socket socket;
    private BufferedReader br;
    private PrintWriter pw;

    String incomingMessage;

    private PropertyChangeSupport propertyChangeSupport = new PropertyChangeSupport(this);

    public Connection(PropertyChangeListener propChangeListener, Role role, String host, int port) {
        propertyChangeSupport.addPropertyChangeListener(propChangeListener);
        this.role = role;
        this.host = host;
        this.port = port;
    }

    @Override
    public void run() {
        if (role == Role.SERVER) {
            connectAsServer();
            waitForMessage();
        } else if (role == Role.CLIENT) {
            waitForServer();
            waitForMessage();
        }
    }

    private void waitForServer() {
        System.out.println("Waiting for server...");
        try {
            while (!Thread.interrupted() && socket == null) {
                connectAsClient();
                Thread.sleep(1_000);
            }
        } catch (InterruptedException ex) {
            System.out.println("Waiting for server interrupted.");
        }
    }

    public void connectAsClient() {
        try {
            socket = new Socket(host, port);
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            pw = new PrintWriter(socket.getOutputStream(), true);
            System.out.println("Client connected...");
            propertyChangeSupport.firePropertyChange("socket", null, socket);
        } catch (IOException ex) {
            System.err.println("Can't access host " + host + " and port " + port);
        }
    }

    private void connectAsServer() {
        try {
            ss = new ServerSocket(port);
            socket = ss.accept();
            br = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            pw = new PrintWriter(socket.getOutputStream(), true);
            System.out.println("Server connected...");
            propertyChangeSupport.firePropertyChange("socket", null, socket);
        } catch (IOException ex) {
            System.err.println("Can't access port " + port + " or server socked closed before connected");
        }
    }

    public void waitForMessage() {
        while (!Thread.interrupted()) {
            try {
                incomingMessage = br.readLine();
                propertyChangeSupport.firePropertyChange("incomingMessage", "", incomingMessage);
            } catch (IOException ex) {
                System.err.println("Error while waiting for a message");
            }
        }
    }

    public void sendMessage(String message) {
        pw.println(message);
    }

    public void disconnect() {
        try {
            if (role == Role.SERVER) {
                ss.close();
            }
            if (br != null) {
                br.close();
            }
            if (pw != null) {
                pw.close();
            }
            if (socket != null) {
                socket.close();
            }
        } catch (IOException ex) {
            System.out.println("Problems with closing network channels");
        }
    }
}
